#include <polynomial/InterpPolynomial.hpp>


int main(){

  int n = 5, m = 1;
  Matrix x(n,1),y(n,1);
  
  x.set(0,0,-1);
  x.set(1,0,1);
  x.set(2,0,5);
  x.set(3,0,9);
  x.set(4,0,90);

  y.set(0,0,4);
  y.set(1,0,-2);
  y.set(2,0,10);
  y.set(3,0,-2);
  y.set(4,0,10);

  InterpPolynomial ip(x, y);

  return 0;
}
