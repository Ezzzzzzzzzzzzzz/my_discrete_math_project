#include <polynomial/Polynomial.hpp>


void Polynomial::print(){

  for(int i = 0; i < nMonomials; ++i){

    if (pol[i].val < 0){
        cout << pol[i].val << pol[i].var_name;
    }
    else{
        cout << "+" << pol[i].val << pol[i].var_name;
    }

    if( pol[i].exp > 0 ){
      cout << "^" << pol[i].exp;
    }
    cout << " " ;
  }
  cout << endl;
}

void Polynomial::addMonomial(float co, char lit, int exp){

  if( nMonomials < MAX_ELEMS){
    
   pol[nMonomials].val = co;
    if( exp > 0){
      pol[nMonomials].var_name = lit;
      pol[nMonomials].exp = exp;
    }
    else{
      pol[nMonomials].exp = 0;
    }
    nMonomials++;
  }
}

