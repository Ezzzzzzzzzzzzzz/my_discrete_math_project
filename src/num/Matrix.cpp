#include <num/Matrix.hpp>


bool Matrix::set(int i, int j, float val){
  if( i< MAX_ROWS && j< MAX_COLS){
    mat[i][j] = val;
    return true;
  }  
  return false;
}

bool Matrix::get(int i, int j, float &val){
  if( i< MAX_ROWS && j< MAX_COLS){
    val = mat[i][j];
    return true;
  }  
  return false;
}

void Matrix::print(){
  cout << " "<< endl;
  for(int i=0; i < _rows; ++i){
    for(int j=0; j < _cols; ++j)
      cout << mat[i][j] << " ";
  
    cout << endl;
  }
  cout << endl;
} 


Matrix Matrix::eGaussian(Matrix &x1){
  float det = 1;
  float a, p, p1, x, y, z, w, s, r, o, res;
  Matrix x2(x1.rows(), x1.cols());

  for(int i = 0; i < rows(); i++){
    get(i,i,p);
    
    for(int j = i + 1; j < rows(); j++){
      get(j,i,p1);
      a = p1/p;
      
      for(int k = 0; k < cols(); k++){
        get(j,k,x);
        get(i,k,y);
        x = x - a*y; 
        set(j,k,x);
      }
    }
  }

  get(rows()-1,cols()-1,z); 
  get(rows()-1, cols()-2, w);
  
  x2.set(rows()-1,0, z/w);

  for(int i=rows()-2; i>=0; i--){
    get(i,cols()-1,r);
    //cout << "r:" << r << endl;
    s = r;
    for(int j=i+1; j<rows(); j++){
      get(i,j,o);
      //cout << "o:" << o << endl;
      x2.get(j,0,p);
      //cout << "p:" << p << endl;
      s = s-(o*p);
      //cout << "s:" << s << endl;
    }
    get(i,i,y);
    //cout << "y:" << y << endl;
    res = s/y;
    x2.set(i,0,res);
  }
  return x2;
}
