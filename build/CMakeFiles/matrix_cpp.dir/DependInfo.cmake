# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ezz/my_discrete_math_project/examples/matrix/matrix.cpp" "/home/ezz/my_discrete_math_project/build/CMakeFiles/matrix_cpp.dir/examples/matrix/matrix.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/ezz/my_discrete_math_project/build/CMakeFiles/num_lib.dir/DependInfo.cmake"
  "/home/ezz/my_discrete_math_project/build/CMakeFiles/utils_lib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
