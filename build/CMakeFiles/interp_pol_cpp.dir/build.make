# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ezz/my_discrete_math_project

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ezz/my_discrete_math_project/build

# Include any dependencies generated for this target.
include CMakeFiles/interp_pol_cpp.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/interp_pol_cpp.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/interp_pol_cpp.dir/flags.make

CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o: CMakeFiles/interp_pol_cpp.dir/flags.make
CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o: ../examples/interp_pol/interp_pol.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/ezz/my_discrete_math_project/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o"
	/usr/bin/g++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o -c /home/ezz/my_discrete_math_project/examples/interp_pol/interp_pol.cpp

CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.i"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/ezz/my_discrete_math_project/examples/interp_pol/interp_pol.cpp > CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.i

CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.s"
	/usr/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/ezz/my_discrete_math_project/examples/interp_pol/interp_pol.cpp -o CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.s

CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o.requires:

.PHONY : CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o.requires

CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o.provides: CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o.requires
	$(MAKE) -f CMakeFiles/interp_pol_cpp.dir/build.make CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o.provides.build
.PHONY : CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o.provides

CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o.provides.build: CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o


# Object files for target interp_pol_cpp
interp_pol_cpp_OBJECTS = \
"CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o"

# External object files for target interp_pol_cpp
interp_pol_cpp_EXTERNAL_OBJECTS =

bin/interp_pol_cpp: CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o
bin/interp_pol_cpp: CMakeFiles/interp_pol_cpp.dir/build.make
bin/interp_pol_cpp: lib/libnum_lib.a
bin/interp_pol_cpp: lib/libutils_lib.a
bin/interp_pol_cpp: lib/libpolynomial_lib.a
bin/interp_pol_cpp: CMakeFiles/interp_pol_cpp.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/ezz/my_discrete_math_project/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable bin/interp_pol_cpp"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/interp_pol_cpp.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/interp_pol_cpp.dir/build: bin/interp_pol_cpp

.PHONY : CMakeFiles/interp_pol_cpp.dir/build

CMakeFiles/interp_pol_cpp.dir/requires: CMakeFiles/interp_pol_cpp.dir/examples/interp_pol/interp_pol.cpp.o.requires

.PHONY : CMakeFiles/interp_pol_cpp.dir/requires

CMakeFiles/interp_pol_cpp.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/interp_pol_cpp.dir/cmake_clean.cmake
.PHONY : CMakeFiles/interp_pol_cpp.dir/clean

CMakeFiles/interp_pol_cpp.dir/depend:
	cd /home/ezz/my_discrete_math_project/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/ezz/my_discrete_math_project /home/ezz/my_discrete_math_project /home/ezz/my_discrete_math_project/build /home/ezz/my_discrete_math_project/build /home/ezz/my_discrete_math_project/build/CMakeFiles/interp_pol_cpp.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/interp_pol_cpp.dir/depend

