#ifndef POLYNOMIAL_HPP
#define POLYNOMIAL_HPP

#include <iostream>
#include <stdlib.h> 

using namespace std;
#define MAX_ELEMS  100

struct Monomial{
    float val;
    char var_name;    // 'x'
    int exp;         //x^exp
    char sign;
};

class Polynomial{

  Monomial pol[MAX_ELEMS];
  int nTerms;
  float a;
  float b;
  int nMonomials;
  
  public:

    Polynomial(){nMonomials = 0;} 
    void print(); 
    void addMonomial(float co, char lit, int exp);
  
};

#endif
