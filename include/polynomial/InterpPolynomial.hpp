
#ifndef INTERPOLYNOMIAL_HPP
#define INTERPOLYNOMIAL_HPP

#include <iostream>
#include <cmath>
#include <polynomial/Polynomial.hpp>
#include <num/Matrix.hpp>

using namespace std;

class InterpPolynomial: public Polynomial{
    int val = 0;
    int grado = 0;
    int m_Grado = 0;
    float a, b, coef;
   
  public:

  InterpPolynomial(Matrix &x, Matrix &y){
  
    Matrix vec_x = x;
    Matrix vec_y = y;
    Matrix vander(vec_x.rows(), vec_x.rows()+1); 

    cout << "X: " << endl;
    vec_x.print();
    
    cout << "Y: " << endl;
    vec_y.print();

    for(int i = 0; i < vec_x.rows(); i++){
      grado = vec_x.rows()-1;
      for(int j = 0; j < vec_x.rows(); j++){

        vec_x.get(i,0,a);
        val = pow(a,grado);           
        vander.set(i,j,val);
        grado--;
      }
    }
    
    for(int i = 0; i < vander.rows(); i++){
      for(int j = vander.cols()-1; j < vander.cols(); j++){
        vec_y.get(i,0,b);
        vander.set(i,j,b);
      }

    }


  cout << "Matriz de Vendermonde: " << endl;
  vander.print();

  Matrix v_coef = vander.eGaussian(vec_y);

  for(int i = v_coef.rows()-1; i >= 0; i--){
    v_coef.get(i,0,coef);
    addMonomial(coef, 'x', m_Grado);
    m_Grado++;
  }
  
  cout << "El polinomio de interpolación es: " << endl;
  cout << endl;
  
  cout <<"F(x) = ";
  print();
  cout << endl;

  };
};

#endif
