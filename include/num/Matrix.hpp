
#ifndef MATRIX_HPP
#define MATRIX_HPP

#include<iostream>
#include<cstdlib>
#include<cmath>

using namespace std;
#define MAX_ROWS  100
#define MAX_COLS  100


class Matrix {
  float mat[MAX_ROWS][MAX_COLS];              // Arreglo bidimensional
  int  _rows;                                 // No. de renglones
  int  _cols;                                 // No. de columnas     

public:
  
  int rows(){return _rows;}
  int cols(){return _cols;}
  Matrix(int n, int m){_rows = n; _cols = m;}
  bool set(int,int,float);
  bool get(int,int,float &);
  void print();
  Matrix eGaussian(Matrix &);                   //Implementar, eliminación Guassiana
};

#endif